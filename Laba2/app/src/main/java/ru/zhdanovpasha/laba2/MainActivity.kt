package ru.zhdanovpasha.laba2

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.design.widget.Snackbar.LENGTH_LONG
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var a : String
    private lateinit var b : String
    private lateinit var snackbar: Snackbar
    private lateinit var firstText:EditText
    private lateinit var secondText:EditText
    private lateinit var plusBtn:Button
    private lateinit var minusBtn:Button
    private lateinit var multiplyBtn:Button
    private lateinit var deleteBtn:Button
    private lateinit var infoText:TextView

    private val DIV_BY_ZERO = "Division by zero"
    private val INPUT_BOTH_NUM = "Input both numbers"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        firstText = first_edit_text
        secondText = second_edit_text
        plusBtn = plus_button
        minusBtn = minus_button
        multiplyBtn = multiply_button
        deleteBtn = delete_button
        infoText = info_text

        coordinatorLayout = findViewById(R.id.coordinator)
        snackbar = Snackbar.make(coordinatorLayout, INPUT_BOTH_NUM, LENGTH_LONG)

        plusBtn.setOnClickListener{
            infoText.text =""
            getNumbers()
            if (checkEmpty()){
                showSnackbar(INPUT_BOTH_NUM)
            } else{
                infoText.text = ("Result is: " + (a.toDouble() + b.toDouble()).toString())
            }
        }

        minusBtn.setOnClickListener {
            infoText.text =""
            getNumbers()
            if (checkEmpty()){
                showSnackbar(INPUT_BOTH_NUM)
            } else {
                infoText.text = ("Result is: " + (a.toDouble() - b.toDouble()).toString())
            }
        }

        multiplyBtn.setOnClickListener{
            infoText.text =""
            getNumbers()
            if (checkEmpty()){
                showSnackbar(INPUT_BOTH_NUM)
            } else {
                infoText.text = ("Result is: " + (a.toDouble() * b.toDouble()).toString())
            }
        }

        deleteBtn.setOnClickListener{
            infoText.text =""
            getNumbers()
            if (checkEmpty()){
                showSnackbar(INPUT_BOTH_NUM)
            } else {
                if (b == "0"){
                    showSnackbar(DIV_BY_ZERO)
                } else {
                    infoText.text = ("Result is: " + (a.toDouble() / b.toDouble()).toString())
                }
            }
        }
    }

    private fun getNumbers(){
        a = first_edit_text.text.toString()
        b = second_edit_text.text.toString()
    }

    private fun checkEmpty():Boolean{
        return a.isEmpty() || b.isEmpty()
    }

    private fun showSnackbar(str: String){
        snackbar.setText(str)
        snackbar.show()
    }

}
